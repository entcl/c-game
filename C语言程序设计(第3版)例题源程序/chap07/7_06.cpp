/*【例7-6】调查电视节目欢迎程度。某电视台要进行一次对该台8个栏目（设相应栏目编号为1~8）的受欢迎情况，共调查了1000位观众，现要求编写程序，输入每一位观众的投票，每位观众只能选择一个最喜欢的栏目投票，统计输出各栏目的得票情况。*/

/* 投票情况统计 */
# include<stdio.h>
int main( void ) 
{
    int count[9];							/* 设立数组，栏目编号对应数组下标 */
    int i,response;

    for(i = 1;i <= 8;i++)
        count[i] = 0;						/* 各栏目计数器清0 */
    for( i = 1;i <= 10;i++) {				/* 为调试运行方便，可把1000改小，如10 */
        printf("input your response: ");	/* 输入提示 */
        scanf("%d",&response);
        if(response < 1 || response > 8)	/* 检查投票是否有效*/
            printf("this is a bad response: %d\n",response);
        else
            count[response]++;				/* 对应栏目得票加1 */
    }

    printf("result:\n");					/* 输出各栏目得票情况 */
    for(i = 1;i <= 8;i++)
        printf("%4d%4d\n",i,count[i]);	

    return 0;
}
